package eval;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class test {

	@SuppressWarnings("serial")
	public static void main (String[] args){
		/**
		 * Exercice 1
		 */
		RegexTester("^(?=[^A-Z]*[A-Z]{1}[^A-Z]*$)(?=[^-]*-[^-]*-[^-]*$).*$",new ArrayList<String>(){{
	        add("A-b-c");
	        add("R--c");
	        add("A-B-c");
	        add("A-qsmldkfj qsddfkjqsd s qfmlkqsjf qsdml fkjq_çza -c");
	    }});
		
		/**
		 * Exercice 2
		 */
		RegexTester("\\b[0-9]*1[0-9]*2[0-9]*3[0-9]*(?<=[13579])\\b",new ArrayList<String>(){{
	        add("3142537");
	        add("314253");
	        add("1323");
	        add("qsdf 112223335  abcd");
	        add("qsdf 112223334  abcd");
	        add("15");
	    }});//Exercice 1
		
	}
	
	public static void RegexTester(String param, ArrayList<String> param2) {
	    String patternString = param;
	    for (int i = 0; i < param2.size(); i++) {
	        String text = param2.get(i);
	        Pattern p = Pattern.compile(patternString);
	        Matcher m = p.matcher(text);
	        System.out.println("R.E.: " + patternString);
	        System.out.println("Test: " + text);
	        boolean found = m.find();
	        if (found) {
	            System.out.println("Position début : " + m.start());
	            System.out.println("Avant : " + text.substring(0,m.start()));
	            System.out.println("Sélection : " + m.group());
	            // System.out.println("Groupe : " + m.group(1));
	            System.out.println("Position fin : " + m.end());
	            System.out.println("Après : " + text.substring(m.end()));

	            }else{
	            	System.out.println("No Match found");
	            }
	        System.out.println("\n");
	    }
	}
	
}
